const Goods = artifacts.require('Goods');
const Manufacturers = artifacts.require('Manufacturers');

module.exports = function (deployer) {
  deployer.deploy(Goods);
  deployer.deploy(Manufacturers);
};
