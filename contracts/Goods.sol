// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import {Manufacturers, Manufacturer} from "./Manufacturers.sol";
contract Goods {
  
  struct Product {
    string _serial_no;
    string name;
    string category;
    string mfg_date;
    string manufacturer;
    bool sold;
  }

  uint public productsCount = 0;
  mapping(string => Product) public products;

  event createProductEvent (
    string name,
    string cateogry,
    string mfg_date,
    string manufacturer,
    bool sold
  );

  event soldEvent (string serial_no, bool sold);


   /* function createProduct(string memory serial_no, string memory name, string memory category, string memory mfg_date, string memory manufacturer)
   public returns (Product memory) {
    
    //Ensure we are not using the same searial number twice
     require(bytes(products[serial_no]._serial_no).length == 0, "Product with this serial no. already exists");
     
      productsCount++;
      //Verify that the manufacturer is allowed to create this produce with the given manufacturer id
      products[serial_no] = Product(serial_no, name, category, mfg_date, manufacturer);
     emit createProductEvent(name, category, mfg_date, manufacturer);
     return products[serial_no];
   } 
 */
   function createVerifiedProduct(string memory serial_no, string memory name, string memory category, string memory mfg_date, string memory manufacturer, Manufacturers man_contract)
   public returns (Product memory) {
    
    //Ensure we are not using the same searial number twice
     require(bytes(products[serial_no]._serial_no).length == 0,"Product with this serial no. already exists");

     Manufacturer memory man = man_contract.findManufacturer(msg.sender);

     require(bytes(man.code).length!=0, "Manufacturer not found");

     require(keccak256(abi.encodePacked(man.code))== keccak256(abi.encodePacked(manufacturer)), "Invalid manufacturer");
     
      //Verify that the manufacturer is allowed to create this produce with the given manufacturer id
      products[serial_no] = Product(serial_no, name, category, mfg_date, manufacturer, false);
       productsCount++;
     emit createProductEvent(name, category, mfg_date, manufacturer,false);
     return products[serial_no];
   }

   function markAsSold(string memory serial_no) public returns (Product memory) {
      require(bytes(products[serial_no]._serial_no).length!=0, "Product not found");
      products[serial_no].sold=true;
      emit soldEvent(serial_no, true);
      return products[serial_no];
   } 

   function findProduct(string memory serial_no) public view  returns ( Product memory) {
     return products[serial_no];
   }
}