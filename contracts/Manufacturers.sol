// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


struct Manufacturer {
    string name;
    string code;
  }

contract Manufacturers {
  
  uint public manufacturersCount = 0;
  mapping(address => Manufacturer) public manufacturers;

  event createManufactureerEvent (
    string name,
    string code
  );

   function createManufacturer(string memory name, string memory code)
   public returns (Manufacturer memory) {
      //Verify that the manufacturer is allowed to create this produce with the given manufacturer id
      manufacturers[msg.sender] = Manufacturer(name, code);
       manufacturersCount++;
     emit createManufactureerEvent(name,code);
     return manufacturers[msg.sender];
   }  

   function findManufacturer(address addr)
   external view  returns (Manufacturer memory) {
    return manufacturers[addr];
   }
}
