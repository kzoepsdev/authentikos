import {
	Avatar,
	Box,
	Button,
	Center,
	Flex,
	FormControl,
	FormLabel,
	Heading,
	Image,
	Input,
	Link, Spinner,
	Stack,
	Text,
	useToast,
} from '@chakra-ui/react';
import React, {useEffect, useState} from 'react';
import {useAccount, useContractRead, useContractWrite} from "wagmi";
import {MANUFACTURERS_ABI, MANUFACTURERS_CONTRACT_ADDRESS} from "./abi/config_manufacturers";
import {useNavigate} from "react-router-dom";

export function SignUp1(props) {

	const [name, setName] = useState();
	const [code, setCode] = useState();
	const [loading, setLoading] = useState(true);
	const [manufacturerDets, setManufacturerDets] = useState();
	const toast = useToast();
	const {data} = useAccount();
	const navigate = useNavigate();
	const {isLoading, write} = useContractWrite({
		addressOrName: MANUFACTURERS_CONTRACT_ADDRESS,
		contractInterface: MANUFACTURERS_ABI
	}, 'createManufacturer', {
		onSuccess(data) {
			console.log(data);
			toast({
				title: 'Successfully created manufacturer',
				status: 'success'
			})
		}
	});

	const web3 = new window.Web3(
		window.Web3.givenProvider || 'https://ropsten.infura.io/v3/9b2a0b8ec0dc4bd5819d292b78c0b7ac'
	);

	const manuContract = new web3.eth.Contract(
		MANUFACTURERS_ABI, MANUFACTURERS_CONTRACT_ADDRESS
	);
	const getDetails = async () => {
		const result = await manuContract.methods.findManufacturer(data?.address).call()
		setLoading(false);
		setManufacturerDets(result);
	}

	useEffect(() => {
		console.log(data?.address);
		if (data?.address) void getDetails();
	}, [data?.address])

	const createManufacturer = async () => {
		setLoading(true);
		const data = await write({args: [name, code]});
		navigate('/product')
	}

	if (manufacturerDets?.name && manufacturerDets?.code || loading) {
		return (<>
			<Center py={6}>
				<Box
					maxW={'270px'}
					w={'full'}
					bg={'white'}
					boxShadow={'2xl'}
					rounded={'md'}
					overflow={'hidden'}>
					<Image
						h={'120px'}
						w={'full'}
						src={
							'https://images.unsplash.com/photo-1612865547334-09cb8cb455da?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80'
						}
						objectFit={'cover'}
					/>
					{loading ? <Spinner/> : <>
						<Flex justify={'center'} mt={-12}>
							<Avatar
								size={'xl'}
								src={
									'https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&q=80&fm=jpg&crop=faces&fit=crop&h=200&w=200&ixid=eyJhcHBfaWQiOjE3Nzg0fQ'
								}
								alt={'Author'}
								css={{
									border: '2px solid white',
								}}
							/>
						</Flex>

						<Box p={6}>
							<Stack spacing={0} align={'center'} mb={5}>
								<Heading fontSize={'2xl'} fontWeight={500} fontFamily={'body'}>
									{manufacturerDets?.name}
								</Heading>
								<Text color={'gray.500'}>{manufacturerDets?.code}</Text>
							</Stack>
						</Box>
					</>
					}
				</Box>
			</Center>
		</>)
	}
	return (
		<Flex
			minH={'100vh'}
			align={'center'}
			justify={'center'}
			bg={'gray.50'}>
			<Stack spacing={8} mx={'auto'} maxW={'lg'} py={4} px={6}>
				<Stack align={'center'}>
					<Heading fontSize={'4xl'} textAlign={'center'}>
						Create Manufacturer
					</Heading>
					<Text fontSize={'lg'} color={'gray.600'}>
						to enjoy all of our cool features ✌️
					</Text>
				</Stack>
				<Box
					rounded={'lg'}
					bg={'white'}
					boxShadow={'lg'}
					p={8}>
					<Stack spacing={4}>
						<FormControl id="email" isRequired>
							<FormLabel>Manufacturer Name</FormLabel>
							<Input width={'sm'} onChange={(event) => setName(event.target.value)}/>
						</FormControl>
						<FormControl id="code" isRequired>
							<FormLabel>Manufacturer Code</FormLabel>
							<Input onChange={(event) => setCode(event.target.value)}/>
						</FormControl>
						<Stack spacing={10} pt={2}>
							<Button
								onClick={createManufacturer}
								isLoading={isLoading}
								loadingText="Submitting"
								size="lg"
								bg={'blue.400'}
								color={'white'}
								_hover={{
									bg: 'blue.500',
								}}>
								Submit
							</Button>
						</Stack>
					</Stack>
				</Box>
			</Stack>
		</Flex>
	);
}
