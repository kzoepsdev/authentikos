import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Product from './components/Products'
import {ChakraProvider} from '@chakra-ui/react';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import WithSubnavigation from "./components/navigation";
import {SignUp1} from './sign-up';
import Search from "./search";
import View from "./view";
import {configureChains, createClient, defaultChains, chain, WagmiConfig} from 'wagmi';
import { jsonRpcProvider } from 'wagmi/providers/jsonRpc';
import {publicProvider} from 'wagmi/providers/public';
import { infuraProvider } from 'wagmi/providers/infura';
import {InjectedConnector} from "wagmi/connectors/injected";
import AddressProvider from "./address-provider";

const infuraId = "9b2a0b8ec0dc4bd5819d292b78c0b7ac"

const {chains, provider, webSocketProvider} = configureChains([chain.ropsten], [
	infuraProvider({ infuraId }),
	publicProvider(),
	// jsonRpcProvider({
	// 	rpc: (chain) => ({
	// 		http: `http://localhost:7545`,
	// 	}),
	// }),
])

const client = createClient({
	autoConnect: true,
	connectors: [
		new InjectedConnector({
			chains,
			options: {
				name: 'Injected',
				shimDisconnect: true,
			},
		}),
	],
	provider,
	webSocketProvider,
});

const root = (document.getElementById('root'));
ReactDOM.render(
	<React.StrictMode>
		<WagmiConfig client={client}>
			<ChakraProvider>
				<AddressProvider>
					<BrowserRouter>
						<WithSubnavigation/>
						<Routes>
							<Route path="/" element={<App/>}/>
							<Route path="/sign-up" element={<SignUp1/>}/>
							<Route path="/search" element={<Search/>}>
								<Route path=":id" element={<View/>}/>
							</Route>
							<Route path="/product" element={<Product/>}/>
						</Routes>
					</BrowserRouter>
				</AddressProvider>
			</ChakraProvider>
		</WagmiConfig>
	</React.StrictMode>
	, root);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
