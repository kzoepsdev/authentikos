import {Button, Center, Flex, HStack, Input, InputGroup, InputLeftElement, VStack} from "@chakra-ui/react";
import {SearchIcon} from "@chakra-ui/icons";
import {Outlet, useNavigate} from "react-router-dom";
import {useState} from "react";

export default function Search() {

	const [search, setSearch] = useState(undefined);
	const navigate = useNavigate();

	const handleSearch = () => {
		navigate(`/search/${search}`);
	}
	return (
		<>
			<Center w="100%" bg="gray.50">
				<Flex minH="100vh" align="center" justify="center" bg="gray.50">
					<VStack mt={8} mb={12}>
						<HStack w={'3xl'} mb={6} pl={0}>
							<InputGroup>
								<InputLeftElement
									pointerEvents="none"><SearchIcon color="black"/></InputLeftElement>
								<Input onChange={(val) => {
									setSearch(val.target.value)
								}} boxShadow={'md'} bg={'white'} variant='filled' type="tel"
									   placeholder="Serial Number"/>
							</InputGroup>
							<Button
								/* flex={1} */
								onClick={handleSearch}
								px={4}
								fontSize={'sm'}
								bg={'green.400'}
								color={'white'}
								boxShadow={
									'0px 1px 25px -5px rgb(66 153 225 / 48%), 0 10px 10px -5px rgb(66 153 225 / 43%)'
								}
								_hover={{
									bg: 'green.500'
								}}
								_focus={{
									bg: 'green.500'
								}}>
								Search
							</Button>
						</HStack>
						<Outlet/>
					</VStack>
				</Flex>
			</Center>
		</>
	)
}
