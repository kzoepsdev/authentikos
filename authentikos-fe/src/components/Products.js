import {
	Button,
	Checkbox,
	Modal,
	ModalBody,
	ModalCloseButton,
	ModalContent,
	ModalHeader,
	ModalOverlay,
	Table,
	TableContainer,
	Tbody,
	Td,
	Th,
	Thead,
	Tr
} from "@chakra-ui/react";
import {useState} from "react";
import ProductModalContent from './ModalData';
import {useContractWrite} from "wagmi";
import { GOODS_ABI, GOODS_CONTRACT_ADDRESS } from "../abi/configs_goods";

const DUMMY_DATA = [
    {
        contractAddress: 'thimphu',
        from: 'somewhere',
        to: 'heaven',
    },
    {
        contractAddress: 'thimphu',
        from: 'somewhere',
        to: 'heaven',
    },
    {
        contractAddress: 'thimphu',
        from: 'somewhere',
        to: 'heaven',
    }
]

export default function Product() {
    const [isOpen, setOpen] = useState(false);
    const onSend = (value) => {
        // CREATE PRIDUCT HERE
        setOpen(!isOpen);
    }

    const close = () => setOpen(!isOpen);
    return (
        <>
            <div style={{ display: "flex", alignItems: "center", justifyContent: "end", paddingRight: "50px", paddingTop: "50px" }}>
                <Button colorScheme='blue' onClick={() => setOpen(!isOpen)}>Add Product</Button>
            </div>
            {/* open modal */}
            <Modal isOpen={isOpen} onClose={!isOpen}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Add Product</ModalHeader>
                    <ModalCloseButton onClick={() => setOpen(!isOpen)} />
                    <ModalBody>
                        <ProductModalContent onSend={onSend} close={close} />
                    </ModalBody>
                </ModalContent>
            </Modal>

            <div className="container">
                <TableContainer m={'50px 50px 50px 50px'} border={"1px solid"} borderRadius={'8px'}>
                    <Table variant='simple'>
                        <Thead>
                            <Tr>
                                <Th> Contract Address</Th>
                                <Th>Sender's Address</Th>
                                <Th>Recipients Address</Th>
                                <Th>Sold</Th>
                            </Tr>
                        </Thead>
                        <Tbody>
                            {
                                DUMMY_DATA.map((item, index) =>
                                    <Tr key={index}>
                                        <Td>{item.contractAddress}</Td>
                                        <Td>{item.from}</Td>
                                        <Td>{item.to}</Td>
                                        <Td><Checkbox defaultIsChecked></Checkbox></Td>
                                    </Tr>
                                )
                            }
                        </Tbody>
                    </Table>
                </TableContainer>
            </div>
        </>
    )
}
