import {
	Button,
	Flex,
	FormControl,
	FormLabel,
	Input,
	Stack,
	Text,
	useColorModeValue,
	useToast,
	VStack,
} from '@chakra-ui/react';
import {Formik} from 'formik';
import {useAccount, useContractRead, useContractWrite, useSignMessage} from "wagmi";
import {GOODS_ABI, GOODS_CONTRACT_ADDRESS} from "../abi/configs_goods";
import {useEffect, useRef, useState} from "react";
import QRCode from "react-qr-code";
import {MANUFACTURERS_ABI, MANUFACTURERS_CONTRACT_ADDRESS} from "../abi/config_manufacturers";

// const ProductModel = yup.object().shape({
//     name: yup.string().required(),
//     model: yup.string().required(),
//     category: yup.string().required(),
//     mgsDate: yup.date().default(function () {
//         return new Date();
//     }),
// });

export default function ProductModalContent(props) {
	const [state, setFormValues] = useState({});
	const toast = useToast();
	const manuCode = useRef(undefined);
	const {data: accData} = useAccount();
	const {data, isLoading, writeAsync} = useContractWrite({
		addressOrName: GOODS_CONTRACT_ADDRESS,
		contractInterface: GOODS_ABI
	}, 'createVerifiedProduct', {
		onSuccess(data) {
			toast({
				title: 'Created Product successfully',
				status: 'success'
			})
		}
	})
	const web3 = new window.Web3(window.Web3.givenProvider||'https://ropsten.infura.io/v3/9b2a0b8ec0dc4bd5819d292b78c0b7ac');


	const {data: accountData, isLoading: isReading, refetch} = useContractRead({
		addressOrName: MANUFACTURERS_CONTRACT_ADDRESS,
		contractInterface: MANUFACTURERS_ABI
	}, 'findManufacturer', {
		args: [accData?.address || ''],
		onSuccess(data) {
			manuCode.current = data
		},
		onError(err) {
			console.log(err)
		}
	})

	const {data: signatureData, signMessageAsync} = useSignMessage()
	const [qrSignature, setQrSignature] = useState(undefined);

	const handleSubmit = async (values) => {
		const message = `${values.name}, ${values?.manufacturer}`
		const signature = await signMessageAsync({
			message
		});
		state.isLoading = true;
		const data = await writeAsync({
			args: [signature, values.name, values.category, values.mfgDate.toString(), values?.manufacturer, MANUFACTURERS_CONTRACT_ADDRESS]
		});
		setQrSignature(signature);
	}
	return (
		<Flex
			align={'center'}
			justify={'center'}
			bg={useColorModeValue('gray.50', 'gray.800')}>
			<Stack
				spacing={4}
				w={'full'}
				maxW={'md'}
				bg={useColorModeValue('white', 'gray.700')}
				rounded={'xl'}
				boxShadow={'lg'}
				p={6}
			>
				{qrSignature ? <VStack maxW={'400px'} spacing={6}>
						<QRCode value={`http://localhost:3000/search/${signatureData}`}/>
					<div style={{width: '350px'}}>
						<Text>Serial Number: {qrSignature}</Text>
					</div>
					</VStack> :
					<Formik initialValues={{name: '', category: '', mfgDate: new Date(), manufacturer: ''}}
							onSubmit={async (values) => {
								await handleSubmit(values);
								// props['onSend'](values)
							}}>
						{({values, errors, touched, handleSubmit, handleChange}) =>
							<>
								<FormControl id="name" isRequired>
									<FormLabel>Item Name</FormLabel>
									<Input
										name='name'
										placeholder="Item Name"
										_placeholder={{color: 'gray.500'}}
										type="text"
										onChange={handleChange}
										value={values.productName}
									/>
									{errors.name && touched.name && errors.name}

								</FormControl>
								<FormControl id="category" isRequired>
									<FormLabel>Category</FormLabel>
									<Input
										name='category'
										placeholder="Category"
										_placeholder={{color: 'gray.500'}}
										type="text"
										onChange={handleChange}
										value={values.category}
									/>
								</FormControl>

								<FormControl id="mfgDate" isRequired>
									<FormLabel>Manufacture Date</FormLabel>
									<Input
										name='mfgDate'
										placeholder="Manufacture Date"
										_placeholder={{color: 'gray.500'}}
										type="date"
										onChange={handleChange}
										value={values.mfgDate}
									/>
								</FormControl>

								<FormControl id="manufacturer" isRequired>
									<FormLabel>Manufacturer</FormLabel>
									<Input
										name='manufacturer'
										placeholder="Manufacture Code"
										_placeholder={{color: 'gray.500'}}
										type="text"
										onChange={handleChange}
										value={values.manufacturer}
									/>
								</FormControl>

								<Button
									bg={'red.400'}
									color={'white'}
									w="full"
									_hover={{
										bg: 'red.500',
									}}
									onClick={props.close}
								>
									Cancel
								</Button>
								<Button
									bg={'blue.400'}
									color={'white'}
									w="full"
									_hover={{
										bg: 'blue.500',
									}}
									onClick={handleSubmit}
								>
									Add
								</Button>
							</>}
					</Formik>
				}
            </Stack>
        </Flex>
    );
}
