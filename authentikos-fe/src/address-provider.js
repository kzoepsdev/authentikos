import React, {useEffect, useState} from "react";
import {useAccount} from "wagmi";
export const Address = React.createContext({
    address: ''
})

export default function AddressProvider(props) {
    const [address, setAddress] = useState();
    const {data} = useAccount();

    useEffect(() =>{
        if (data?.address) setAddress(data?.address)
    }, [data?.address])


    return (
        <Address.Provider value={{ address }}>
            {props.children}
        </Address.Provider>
    )
}
