import {Box, Button, Center, List, ListIcon, ListItem, Stack, Text} from "@chakra-ui/react";
import {CheckIcon} from "@chakra-ui/icons";
import {Checkbox} from "@chakra-ui/react";
import {useParams} from "react-router-dom";
import {useContract, useContractRead} from "wagmi";
import {useContext, useEffect, useState} from "react";
import {GOODS_CONTRACT_ADDRESS, GOODS_ABI} from "./abi/configs_goods";
import {Address} from "./address-provider";

export default function View() {

	const params = useParams();
	const address = useContext(Address);
	const [loading, setLoading] = useState(true);
	const [errored,setErrored] = useState(false);
	const [product, setProduct] = useState(undefined);
	console.log(params.id);
	const isLoading = false;
	const data = undefined;
	useEffect(() => {console.log(address, 'adddressss')}, [address])

	const contract = useContract({
		addressOrName: GOODS_CONTRACT_ADDRESS,
		contractInterface: GOODS_ABI
	})

	const web3 = new window.Web3(
		window.Web3.givenProvider || 'http://localhost:7545'
	);

	const productContract = new web3.eth.Contract(
		GOODS_ABI, GOODS_CONTRACT_ADDRESS
	)
	// this.state.studentList.methods.markGraduated(cid)
	// 	.send({ from: this.state.account })
	// 	.once('receipt', (receipt) => {
	// 		this.setState({ loading: false })
	// 		this.loadBlockchainData()
	// 	})



	const markAsSold = () => {
		debugger
		productContract.methods.markAsSold(params?.id).send({
			from: address?.address
		}).once('receipt', receipt =>{
			debugger
            get();
        }).catch(e =>{debugger;})
	}

	/*const stuff = useContractRead({
		addressOrName: GOODS_CONTRACT_ADDRESS,
		contractInterface: GOODS_ABI
	}, 'findProduct', {
		args: [params.id.toString()]
	}, {
		onSuccess(data) {
			console.log(data);
			if (!data) setErrored(true);
		},
		onError(data) {
			setErrored(true);
		}
	});
	const data = stuff?.data || undefined;
	const { isLoading, refetch} = stuff

	*/

	const get = async () => {
		const result = await productContract.methods.productsCount().call();
		const getProduct = await productContract.methods.findProduct(params.id).call();
		setLoading(false)
		setProduct(getProduct)
	}

	useEffect( () => {
		void get();
	}, [params.id])

	if (!loading && !product?.name) {
		return (
			<Box textAlign="center" py={10} px={6}>
				<Text fontSize="18px" mt={3} mb={2}>
					404 Product Not Found
				</Text>
				<Text color={'gray.500'} mb={6}>
					The product you're looking for doesnt seem to exist
				</Text>

				<Button
					colorScheme="teal"
					bgGradient="linear(to-r, teal.400, teal.500, teal.600)"
					color="white"
					variant="solid">
					Go to Home
				</Button>
			</Box>
		)
	}

	return (
		<Center py={6}>

			<Box
				maxW={'330px'}
				w={'full'}
				bg={'white'}
				boxShadow={'2xl'}
				rounded={'md'}
				overflow={'hidden'}>
				<Stack
					textAlign={'center'}
					p={6}
					color={'gray.800'}
					align={'center'}>
					<Text
						fontSize={'sm'}
						fontWeight={500}
						bg={'green.50'}
						p={2}
						px={3}
						color={'green.500'}
						rounded={'full'}>
						Product
					</Text>
					<Stack direction={'row'} align={'center'} justify={'center'}>
						<Text fontSize={'3xl'} fontWeight={800}>
							{product?.name}
						</Text>
					</Stack>
				</Stack>
				<Box w={'700px'} bg={'gray.50'} px={6} py={10}>
					<List spacing={3}>
						<ListItem>
							<ListIcon as={CheckIcon} color="green.400"/>
							Category: {product?.category}
						</ListItem>
						<ListItem>
							<ListIcon as={CheckIcon} color="green.400"/>
							Date: {product?.mfg_date}
						</ListItem>
						<ListItem>
							<ListIcon as={CheckIcon} color="green.400"/>
							Manufacturer: {product?.manufacturer}
						</ListItem>
						<ListItem>
							<Checkbox disabled={true} isChecked={product?.sold}>Is sold ?</Checkbox>
						</ListItem>
					</List>
				</Box>
				<Button colorScheme='blue' margin={'50px'} onClick={markAsSold} disabled={product?.sold}>Mark as Sold</Button>

			</Box>
		</Center>
	)
}
